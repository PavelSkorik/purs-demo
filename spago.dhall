{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "purs-demo"
, dependencies =
  [ "console", "effect", "flare", "halogen", "halogen-vdom", "psci-support" ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
