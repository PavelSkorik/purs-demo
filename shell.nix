with import <nixpkgs> {};


mkShell {
  name = "PurescriptEnv";
  buildInputs = [
    purescript
    nodePackages.pulp
    nodePackages.bower
    spago
    dhall
    dhall-json
  ];
}
